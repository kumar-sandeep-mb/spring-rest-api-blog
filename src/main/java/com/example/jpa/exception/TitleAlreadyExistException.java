package com.example.jpa.exception;

public class TitleAlreadyExistException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public TitleAlreadyExistException(String message) {
        super(message);
    }
}
