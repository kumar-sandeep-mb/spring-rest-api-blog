package com.example.jpa.exception;

public class PageNumberExceedTotalPageException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public PageNumberExceedTotalPageException(String message) {
        super(message);
    }
}
