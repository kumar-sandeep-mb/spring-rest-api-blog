package com.example.jpa.exception;

public class EmptyStringException extends RuntimeException{
    private static final long serialVersionUID = 5861310537366287163L;

    public EmptyStringException(String message) {
        super(message);
    }
}
