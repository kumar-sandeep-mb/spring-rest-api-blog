package com.example.jpa.exception;

public class PasswordMismatchException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public PasswordMismatchException(String message) {
        super(message);
    }
}
