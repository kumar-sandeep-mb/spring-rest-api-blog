package com.example.jpa.exception;

public class InvalidEmailAddressException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public InvalidEmailAddressException(String message) {
        super(message);
    }
}
