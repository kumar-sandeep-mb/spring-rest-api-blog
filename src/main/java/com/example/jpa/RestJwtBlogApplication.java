package com.example.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class RestJwtBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestJwtBlogApplication.class, args);
    }

}
