package com.example.jpa.validation;

import com.example.jpa.dto.AuthorDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(final PasswordMatches constraintAnnotation) {
        //
    }

    @Override
    public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
        final AuthorDto user = (AuthorDto) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }

}
