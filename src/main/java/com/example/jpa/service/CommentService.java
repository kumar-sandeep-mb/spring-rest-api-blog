package com.example.jpa.service;

import com.example.jpa.model.Author;
import com.example.jpa.model.Comment;
import com.example.jpa.model.Post;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.CommentRepository;
import com.example.jpa.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CommentService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    CommentRepository commentRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    AuthorRepository authorRepository;

    public ResponseEntity<?> findCommentByPostId(Long postId){
        LOGGER.debug("rendering getCommentByPostId");
        if(!postRepository.findById(postId).isPresent()){
            LOGGER.error("post does not exist with postId {}",postId);
            ResponseEntity.badRequest().body("invalid postId");
        }
        if(commentRepository.findByPostId(postId) == null){
            ResponseEntity.ok().body("no comment found for this post");
        }
        LOGGER.debug("comments for postId "+postId+" returned successfully");
        return ResponseEntity.ok().body(commentRepository.findByPostId(postId));
    }

    public ResponseEntity<?> saveComment(Comment comment, Authentication authentication, Long postId){
        LOGGER.debug("rendering saveComment");
        if(comment.getCommentMessage().trim().isEmpty()){
            LOGGER.error("comment is empty");
            return ResponseEntity.badRequest().body("comment is empty");
        }
        User user = (User)authentication.getPrincipal();
        comment.setAuthor(authorRepository.findByEmail(user.getUsername()));
        Post post = postRepository.findById(postId).get();
        comment.setPost(post);
        LOGGER.debug("comment saved successfully {}",comment);
        return ResponseEntity.ok().body("comment saved "+ commentRepository.save(comment).toString());
    }
    public ResponseEntity<?> updateComment(String commentMessage,Long id,Authentication authentication){
        LOGGER.debug("rendering updateComment");
        if(commentMessage.trim().isEmpty()){
            LOGGER.error("comment is empty");
            return ResponseEntity.badRequest().body("comment is empty");
        }
        if(!commentRepository.findById(id).isPresent()){
            LOGGER.error("comment with id {} does not exist",id);
            return ResponseEntity.badRequest().body("comment with id "+id+" does not exist");
        }
        User user = (User) authentication.getPrincipal();
        if(authorRepository.findByEmail(user.getUsername()).getId() ==
                commentRepository.findById(id).get().getAuthor().getId()) {
            commentRepository.findById(id).get().setCommentMessage(commentMessage);
            LOGGER.debug("comment updated {}",commentRepository.findById(id).get());
            return ResponseEntity.ok().body("update successfully");
        }else{
            LOGGER.error("unauthorized access to comment with id {}", id);
            return ResponseEntity.badRequest().body("you are not authorized to update this comment");
        }
    }
    public ResponseEntity<?> deleteComment(Long id,Authentication authentication){
        LOGGER.debug("rendering deleteComment");
        if(!commentRepository.findById(id).isPresent()){
            LOGGER.error("comment with id {} does not exist",id);
            return ResponseEntity.badRequest().body("comment with id "+id+" does not exist");
        }
        User user = (User) authentication.getPrincipal();
        if(authorRepository.findByEmail(user.getUsername()).getId() ==
                commentRepository.findById(id).get().getAuthor().getId()) {
            commentRepository.delete(commentRepository.findById(id).get());
            LOGGER.debug("comment with id {} deleted",id);
            return ResponseEntity.ok().body("deleted successfully");
        }else{
            LOGGER.error("unauthorized access to comment with id {}", id);
            return ResponseEntity.badRequest().body("you are not authorized to update this comment");
        }
    }

    public ResponseEntity<?> findCommentByCommentId(Long id) {
        LOGGER.debug("rendering findCommentById");
        if (!commentRepository.findById(id).isPresent()) {
            LOGGER.error("comment with id {} does not exist", id);
            return ResponseEntity.badRequest().body("comment with id " + id + " does not exist");
        }
        LOGGER.debug("comment with comment id {} returned", id);
        return ResponseEntity.ok().body(commentRepository.findById(id).get());
    }
}
