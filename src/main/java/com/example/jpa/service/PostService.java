package com.example.jpa.service;

import com.example.jpa.exception.EmptyStringException;
import com.example.jpa.exception.PageNumberExceedTotalPageException;
import com.example.jpa.exception.TitleAlreadyExistException;
import com.example.jpa.model.Post;
import com.example.jpa.model.Tag;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.PostRepository;
import com.example.jpa.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

import static com.example.jpa.configuration.Constant.*;

@Service
@Transactional
public class PostService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    PostRepository postRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    AuthorRepository authorRepository;

    public ResponseEntity<?> getAllPost(int page) {
        LOGGER.debug("rendering getAllPost");
        try {
            if (page < INITIAL_PAGE_NUMBER || page > postRepository
                    .findAllPublishedPost(PageRequest.of(INITIAL_PAGE_NUMBER, PAGE_SIZE,
                            Sort.by("updatedAt").descending())).getTotalPages() - ONE_PAGE) {
                throw new PageNumberExceedTotalPageException("Page number exceeds the total page count");
            }
        }catch (PageNumberExceedTotalPageException pageNumberExceedTotalPageException){
            LOGGER.error("invalid page number: {}",page);
            pageNumberExceedTotalPageException.printStackTrace();
            return ResponseEntity.badRequest().body("No more post available. Invalid page number");
        }
        LOGGER.debug("Page {} of Post returned: {}",page,postRepository.findAllPublishedPost(PageRequest.of(INITIAL_PAGE_NUMBER, PAGE_SIZE,
                Sort.by("updatedAt").descending())));
        return ResponseEntity.ok().body(postRepository
                .findAllPublishedPost(PageRequest.of(INITIAL_PAGE_NUMBER, PAGE_SIZE,
                        Sort.by("updatedAt").descending())));
    }

    public ResponseEntity<?> getPostByAuthor(User user,int page) {
        LOGGER.debug("rendering getPostByAuthor");
        try {
            if (page < INITIAL_PAGE_NUMBER || page > postRepository
                    .findPostByAuthor(authorRepository.findByEmail(user.getUsername()),
                            PageRequest.of(INITIAL_PAGE_NUMBER, PAGE_SIZE,Sort.by("updatedAt").descending()))
                    .getTotalPages() - ONE_PAGE) {
                throw new PageNumberExceedTotalPageException("Page number exceeds the total page count");
            }
        }catch (PageNumberExceedTotalPageException pageNumberExceedTotalPageException){
            LOGGER.error("invalid page number: {}",page);
            pageNumberExceedTotalPageException.printStackTrace();
            return ResponseEntity.badRequest().body("no more post available. Invalid page number");
        }
        LOGGER.debug("Page {} of Author Post returned: {}",page,postRepository.findPostByAuthor(authorRepository.findByEmail(user.getUsername()),
                PageRequest.of(INITIAL_PAGE_NUMBER,PAGE_SIZE,Sort.by("updatedAt").descending())));
        return ResponseEntity.ok().body(postRepository.findPostByAuthor(authorRepository.findByEmail(user.getUsername()),
                PageRequest.of(INITIAL_PAGE_NUMBER,PAGE_SIZE,Sort.by("updatedAt").descending())));
    }

    public ResponseEntity<?> getDraftByAuthor(User user, int page) {
        LOGGER.debug("rendering getDraftByAuthor");
        try {
            if (page < INITIAL_PAGE_NUMBER || page > postRepository
                    .findDraftByAuthor(authorRepository.findByEmail(user.getUsername()),
                            PageRequest.of(page,PAGE_SIZE,Sort.by("updatedAt").descending()))
                    .getTotalPages() - ONE_PAGE) {
                throw new PageNumberExceedTotalPageException("Page number exceeds the total page count");
            }
        }catch (PageNumberExceedTotalPageException pageNumberExceedTotalPageException){
            LOGGER.error("invalid page number: {}",page);
            pageNumberExceedTotalPageException.printStackTrace();
            return ResponseEntity.badRequest().body("no more post available. Invalid page number");
        }
        LOGGER.debug("Page {} of Author Draft returned : {}",page,postRepository.findDraftByAuthor(authorRepository
                .findByEmail(user.getUsername()), PageRequest.of(page,PAGE_SIZE,
                Sort.by("updatedAt").descending())));
        return ResponseEntity.ok().body(postRepository.findDraftByAuthor(authorRepository
                        .findByEmail(user.getUsername()), PageRequest.of(page,PAGE_SIZE,
                Sort.by("updatedAt").descending())));
    }

    public ResponseEntity<?> getPostBySearch(String searchString, int page) {
        LOGGER.debug("rendering getPostBySearch with searchString: {}",searchString);
        try{
            if(searchString.isEmpty()){
                throw new EmptyStringException("SearchString is empty");
            }
            if(page > postRepository.findPostBySearch(searchString,PageRequest.of(page,PAGE_SIZE,
                    Sort.by("updatedAt").descending())).getTotalPages()-ONE_PAGE){
                throw new PageNumberExceedTotalPageException("Page number exceeds the total page count");
            }
        }catch (EmptyStringException emptyStringException){
            LOGGER.error("empty searchString: {}", searchString);
            emptyStringException.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
        catch (PageNumberExceedTotalPageException pageNumberExceedTotalPageException) {
            LOGGER.error("invalid page number: {}", page);
            pageNumberExceedTotalPageException.printStackTrace();
            return ResponseEntity.badRequest().body("No more post available. Invalid page number");
        }

         return ResponseEntity.ok().body(postRepository.findPostBySearch(searchString,PageRequest.of(page,PAGE_SIZE,
                 Sort.by("updatedAt").descending())));
    }

    public ResponseEntity<?> savePost(Post post, User user) {
        LOGGER.debug("rendering savePost");
        try {
            if (post.getTitle().trim().isEmpty()) {
                throw new EmptyStringException("Post title cannot be empty");
            }
            if (post.getDescription().trim().isEmpty()) {
                throw new EmptyStringException("Post description cannot be empty");
            }
            if (post.getContent().trim().isEmpty()) {
                throw new EmptyStringException("Post content cannot be empty");
            }
            if (postRepository.findByTitle(post.getTitle().trim()) != null) {
                throw new TitleAlreadyExistException("A Post with title '" + post.getTitle() + "' already" +
                        "exist that violates unique title constraint");
            }
        } catch (EmptyStringException emptyStringException){
            LOGGER.error("empty string passed in {}",post);
            emptyStringException.printStackTrace();
            return ResponseEntity.badRequest().body("Post cannot be saved. Title, description or " +
                    "content cannot be empty");
        } catch (TitleAlreadyExistException titleAlreadyExistException){
            LOGGER.error("a post with title "+post.getTitle()+
                    " already exist that violates unique title constraint");
            titleAlreadyExistException.printStackTrace();
            return ResponseEntity.badRequest().body("Post cannot be saved. Post with title" +
                    post.getTitle()+ " already exist that violates unique title constraint");
        }
        post.setAuthor(authorRepository.findByEmail(user.getUsername()));
        Set<Tag> tagSet = post.getTags();
        saveTag(tagSet, post);
        LOGGER.debug("new post saved with details: {}",post);
        return ResponseEntity.ok().body(postRepository.save(post));
    }

    public ResponseEntity<?> updatePost(Post updatedPost, Long postId, Authentication authentication) {
        LOGGER.debug("rendering updatePost");
        if(postRepository.findById(postId).isPresent()) {
            try {
                if (updatedPost.getTitle().trim().isEmpty()) {
                    throw new EmptyStringException("Post title cannot be empty");
                }
                if (updatedPost.getDescription().trim().isEmpty()) {
                    throw new EmptyStringException("Post description cannot be empty");
                }
                if (updatedPost.getContent().trim().isEmpty()) {
                    throw new EmptyStringException("Post content cannot be empty");
                }
                if ((postRepository.findByTitle(updatedPost.getTitle().trim()) != null) &&
                        (!postRepository.findById(postId).get().getTitle().equals(updatedPost.getTitle().trim()))) {
                    throw new TitleAlreadyExistException("A Post with title '" + updatedPost.getTitle() + "' already" +
                            "exist in the database which violates unique title constraint");
                }
            } catch (EmptyStringException emptyStringException) {
                LOGGER.error("empty string passed in {}", updatedPost);
                emptyStringException.printStackTrace();
                return ResponseEntity.badRequest().body("post cannot be updated." +
                        " Title, description or content is empty");
            } catch (TitleAlreadyExistException titleAlreadyExistException) {
                LOGGER.error("post cannot be updated. A post with title " + updatedPost.getTitle() +
                        " already exist");
                titleAlreadyExistException.printStackTrace();
                return ResponseEntity.badRequest().body("post cannot be updated." +
                        " A post with title " + updatedPost.getTitle() + " already exista");
            }
            User user = ((User) authentication.getPrincipal());
            if (!user.getUsername().equals(postRepository.findById(postId).get().getAuthor().getEmail())) {
                LOGGER.error("Post not updated with postId {}. Unauthorized access encountered {}",
                        postRepository.findById(postId).get(), user);
                return ResponseEntity.badRequest().body("post cannot be updated. You are not " +
                        "authorized to update this post");
            } else {
                Post post = postRepository.findById(postId).get();
                post.setTitle(updatedPost.getTitle());
                post.setDescription(updatedPost.getDescription());
                post.setContent(updatedPost.getContent());
                Set<Tag> tagSet = updatedPost.getTags();
                saveTag(tagSet, post);
                return ResponseEntity.ok().body("post updated successfully " +
                        postRepository.save(post));
            }
        }
        else{
            LOGGER.error("Post with postId {} does not exist", postId);
            return ResponseEntity.badRequest().body("post cannot be updated. Invalid postId");
        }
    }

    public ResponseEntity<?> deleteById(Long postId,Authentication authentication) {
        if (postRepository.findById(postId).isPresent()) {
            User user = ((User) authentication.getPrincipal());
            if (user.getUsername().equals(postRepository.findById(postId).get().getAuthor().getEmail())) {
                postRepository.delete(postRepository.getOne(postId));
                return ResponseEntity.ok().body("post deleted with postId: " + postId);
            } else {
                LOGGER.error("Post not Deleted with postId {}. Unauthorized access encountered by {}",
                        postRepository.findById(postId).get(), user);
                return ResponseEntity.badRequest().body("your are not authorized to delete this post");
            }
        } else {
            LOGGER.error("Post with postId {} does not exist", postId);
            return ResponseEntity.badRequest().body("post cannot be deleted. Invalid postId");
        }
    }

    public ResponseEntity<?> findById(Long postId) {
        if (postRepository.findById(postId).isPresent()) {
            return ResponseEntity.ok().body(postRepository.findById(postId).get());
        } else {
            LOGGER.error("Post with postId {} does not Exist", postId);
            return ResponseEntity.badRequest().body("No post available. Invalid postId");
        }
    }

    public ResponseEntity<?> getPostByFilter(Long tagId,int page) {
        LOGGER.debug("rendering getPostByFilter");
        if (tagRepository.findById(tagId).isPresent()) {
            return ResponseEntity.ok().body(tagRepository.findPostByTagId(tagId));
        } else {
            LOGGER.error("Tag with tagId {} does not Exist", tagId);
            return ResponseEntity.badRequest().body("No tag available. Invalid tagId");
        }
    }

    private void saveTag(Set<Tag> tagSet, Post post) {
        List<Tag> tagList = tagRepository.findAll();
        boolean flag;
        for (Tag tag : tagSet) {
            flag = true;
            for (Tag oldTag : tagList) {
                if (oldTag.getName().equals(tag.getName())) {
                    post.getTags().remove(tag);
                    post.getTags().add(oldTag);
                    flag = false;
                    break;
                }
            }
            if (flag) {
                if (tag.getName().equals("")) {
                    post.getTags().remove(tag);
                }
            }
        }
    }
}

