package com.example.jpa.service;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.event.OnRegistrationCompleteEvent;
import com.example.jpa.exception.EmptyStringException;
import com.example.jpa.exception.InvalidEmailAddressException;
import com.example.jpa.exception.PasswordMismatchException;
import com.example.jpa.exception.UserAlreadyExistException;
import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import com.example.jpa.model.VerificationToken;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.VerificationTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.jpa.configuration.Constant.API;
import static com.example.jpa.configuration.Constant.EMAIL_PATTERN;

@Service
@Transactional
public class RegistrationService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageSource messages;

    @Autowired
    AuthorService authorService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment env;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ResponseEntity<?> registerAccount(AuthorDto authorDto, HttpServletRequest request) {
        LOGGER.debug("Registering user account with information: {}", authorDto);
        try {
            if (authorDto.getName() == null || authorDto.getName().trim().isEmpty()) {
                throw (new EmptyStringException("Field name is null"));
            } else if (authorDto.getEmail() == null || authorDto.getEmail().trim().isEmpty()) {
                throw (new EmptyStringException("Field Email is null"));
            } else if (authorDto.getPassword() == null || authorDto.getPassword().trim().isEmpty()) {
                throw (new EmptyStringException("Field Password in null"));
            } else if(!isValidEmail(authorDto.getEmail())){
                throw (new InvalidEmailAddressException("Invalid Email Address"));
            } else if (!authorDto.getPassword().trim().equals(authorDto.getMatchingPassword().trim())) {
                throw (new PasswordMismatchException("Password and confirm password mismatch"));
            } else if (authorService.findUserByEmail(authorDto.getEmail()) != null) {
                throw (new UserAlreadyExistException("user already exist"));
            } else {
                Author registered = authorService.registerNewUserAccount(authorDto);
                final String verificationCode = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+API;
                eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), verificationCode));
                LOGGER.debug("Author registered successfully, verification link is sent to" +
                        "your email address: {}", registered);
                return ResponseEntity.badRequest().body("successfully registered you will receive mail shortly");
            }
        } catch (EmptyStringException emptyStringException) {
            LOGGER.error("Could not register. Empty string passed in {}",authorDto);
            emptyStringException.printStackTrace();
            return ResponseEntity.badRequest().body("Name, Email or password filed is empty");
        } catch (InvalidEmailAddressException invalidEmailAddressException){
            LOGGER.error("Could not register. Invalid Email Address in {}",authorDto);
            invalidEmailAddressException.printStackTrace();
            return ResponseEntity.badRequest().body("Invalid email address");
        } catch (PasswordMismatchException passwordMismatchException){
            LOGGER.error("Could not register. Password mismatch in {}",authorDto);
            passwordMismatchException.printStackTrace();
            return ResponseEntity.badRequest().body("Password mismatch");
        } catch (MailSendException mailSendException) {
            LOGGER.error("Could not register. invalid email", mailSendException);
            mailSendException.printStackTrace();
            return ResponseEntity.badRequest().body("Invalid email id");
        }
    }

    public ResponseEntity<?> confirmRegister(String token) {
        LOGGER.debug("rendering registration confirmation");
        final VerificationToken verificationToken = authorService.getVerificationToken(token);
        if (verificationToken == null) {
            LOGGER.error("invalid verification token: {}", token);
            return ResponseEntity.badRequest().body("invalid token");
        }
        final Author author = verificationToken.getAuthor();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            LOGGER.error("token expired: {}", token);
            return ResponseEntity.badRequest().body("token has expired");
        }

        author.setEnabled(true);
        authorService.saveRegisteredUser(author);
        LOGGER.debug("Author verified: {}", author);
        return ResponseEntity.ok().body("Author verified");
    }

    public ResponseEntity<?> resendVerificationMail(HttpServletRequest request, String userEmail) {
        LOGGER.debug("rendering resend Verification mail");
        Author registered = authorService.findUserByEmail(userEmail);
        if (registered == null) {
            LOGGER.error("Author does not exist with email : " + userEmail);
            return ResponseEntity.badRequest().body("author does not exist");
        } else if (registered.isEnabled()) {
            return ResponseEntity.badRequest().body("Author is already verified");
        } else {
            VerificationToken existingToken = verificationTokenRepository.findByAuthor(registered);
            final VerificationToken newToken = authorService.generateNewVerificationToken(existingToken.getToken());
            final Author author = authorService.getUser(newToken.getToken());
            try {
                final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+API;
                final SimpleMailMessage email = constructEmailMessage(appUrl, newToken, author);
                mailSender.send(email);
                LOGGER.debug("Author verified {}", author);
                return ResponseEntity.ok().body("verification link is sent to your mail");
            } catch (final MailAuthenticationException mailAuthenticationException) {
                LOGGER.error("MailAuthenticationException", mailAuthenticationException);
                mailAuthenticationException.printStackTrace();
                return ResponseEntity.badRequest().body("Invalid Email");
            } catch (final Exception exception) {
                LOGGER.error(exception.getLocalizedMessage(), exception);
                exception.printStackTrace();
                return ResponseEntity.badRequest().body("some other exception");
            }
        }
    }

    public ResponseEntity<?> sendPasswordResetToken(HttpServletRequest request, String userEmail){
        LOGGER.debug("rendering resetPassword");
        if(userEmail.trim().isEmpty()){
            LOGGER.error("email is null");
            return ResponseEntity.badRequest().body("empty email");
        } else if(!isValidEmail(userEmail)) {
            LOGGER.error("Invalid email {}",userEmail);
            return ResponseEntity.badRequest().body("invalid email");
        }
        Author author = authorService.findUserByEmail(userEmail);
        if (author == null) {
            LOGGER.error("Author does not exist with email: {}", userEmail);
            return ResponseEntity.badRequest().body("author does not exist");
        }
        final String token = UUID.randomUUID().toString();
        authorService.createPasswordResetTokenForUser(author, token);
        try {
            final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+API;
            final SimpleMailMessage email = constructResetPasswordTokenEmail(appUrl, request.getLocale(), token, author);
            mailSender.send(email);
            LOGGER.debug("Password reset verification mail sent successfully to mail: {}", userEmail);
            return ResponseEntity.ok().body("you will receive password reset token mail shortly");
        } catch (final MailAuthenticationException mailAuthenticationException) {
            LOGGER.error("MailAuthenticationException", mailAuthenticationException);
            mailAuthenticationException.printStackTrace();
            return ResponseEntity.badRequest().body("Invalid email");
        } catch (final Exception exception) {
            LOGGER.error(exception.getLocalizedMessage(), exception);
            exception.printStackTrace();
            return ResponseEntity.badRequest().body("some other error occurred");
        }
    }

    public ResponseEntity<?> updatePassword(String token, String password){
        LOGGER.debug("rendering changePassword");
        if(password.isEmpty() || password == null){
            LOGGER.error("password field is empty");
            return ResponseEntity.badRequest().body("invalid password");
        }
        final PasswordResetToken passToken = authorService.getPasswordResetToken(token);
        final Author author = passToken.getAuthor();
        if (passToken == null) {
            LOGGER.error("invalid token: {}", token);
            return ResponseEntity.badRequest().body("invalid token");
        }
        final Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            LOGGER.error("token expired: {}", token);
            return ResponseEntity.badRequest().body("token expired");
        }
        authorService.changeUserPassword(author, password);
        LOGGER.debug("password changes successfully {}", author);
        return ResponseEntity.ok().body("Password changed successfully");
    }

    public ResponseEntity<?> updateAuthorSetting(AuthorDto authorDto, Authentication authentication,HttpServletRequest request){
        LOGGER.debug("rendering updateAuthorSetting");
        User user = (User)authentication.getPrincipal();
        Author registered = authorRepository.findByEmail(user.getUsername());
        registered.setName(authorDto.getName());
        registered.setPassword(passwordEncoder.encode(authorDto.getPassword()));
        authorRepository.save(registered);
        if(!registered.getEmail().equals(authorDto.getEmail()))
        {
            registered.setEmail(authorDto.getEmail());
            registered.setEnabled(false);
            VerificationToken existingToken = verificationTokenRepository.findByAuthor(registered);
            final VerificationToken newToken = authorService.generateNewVerificationToken(existingToken.getToken());
            final Author author = authorService.getUser(newToken.getToken());
            try {
                final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                final SimpleMailMessage email = constructEmailMessage(appUrl, newToken, author);
                mailSender.send(email);
            } catch (final MailAuthenticationException mailAuthenticationException) {
                LOGGER.error("MailAuthenticationException", mailAuthenticationException);
                mailAuthenticationException.printStackTrace();
                return ResponseEntity.badRequest().body("mail authentication exception");
            } catch (final Exception exception) {
                LOGGER.error(exception.getLocalizedMessage(), exception);
                exception.printStackTrace();
                return ResponseEntity.badRequest().body("some other exception");
            }
        }
        LOGGER.debug("Updated Author settings successfully");
        return ResponseEntity.ok().body("settings updated successfully. verification link sent" +
                "to your mail. verify to continue using: "+registered);
    }

    private final SimpleMailMessage constructEmailMessage(final String contextPath, final VerificationToken newToken, final Author author) {
        final String url = contextPath + "/registrationConfirm?token=" + newToken.getToken();
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject("Registration Confirmation");
        email.setText("click here to verify your account " + " \r\n" + url);
        email.setTo(author.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private final SimpleMailMessage constructResetPasswordTokenEmail(final String contextPath, final Locale locale, final String token, final Author author) {
        final String url = contextPath + "/author/changePassword?&token="+token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(author.getEmail());
        email.setSubject("Reset Password");
        email.setText(message + " \r\n" + url);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private boolean isValidEmail(final String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
