package com.example.jpa.service;

import com.example.jpa.model.AuthenticationRequest;
import com.example.jpa.model.AuthenticationResponse;
import com.example.jpa.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService myUserDetailsService;

    public ResponseEntity<?> createAuthenticationToken(AuthenticationRequest authenticationRequest){
        LOGGER.debug("rendering createAuthenticationToken");
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()));
            final UserDetails userDetails = myUserDetailsService
                    .loadUserByUsername(authenticationRequest.getUsername());
            final String jwt = jwtTokenUtil.generateToken(userDetails);
            LOGGER.debug("Author authenticated with username {}",authenticationRequest.getUsername());
            return ResponseEntity.ok(new AuthenticationResponse(jwt));
        }
        catch (BadCredentialsException badCredentialsException) {
            LOGGER.error("incorrect username or password");
            badCredentialsException.printStackTrace();
            return ResponseEntity.badRequest().body("incorrect username or password");
        }
    }
}
