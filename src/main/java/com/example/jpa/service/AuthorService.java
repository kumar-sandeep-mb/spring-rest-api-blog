package com.example.jpa.service;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import com.example.jpa.model.VerificationToken;
import com.example.jpa.repository.AuthorRepository;
import com.example.jpa.repository.PasswordResetTokenRepository;
import com.example.jpa.repository.RoleRepository;
import com.example.jpa.repository.VerificationTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.UUID;

@Service
@Transactional
public class AuthorService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    public ResponseEntity<?> getAll(){
        return  ResponseEntity.ok().body(authorRepository.findAll());
    }

    public ResponseEntity<?> makeAdmin(Long id) {
        LOGGER.debug("rendering makeAdmin");
        try {
            Author author = authorRepository.getOne(id);
            if (author.getRoles().contains(roleRepository.findByName("ROLE_ADMIN"))) {
                LOGGER.error("author is already a admin");
                return ResponseEntity.badRequest().body("author with id:  " + id + "is already a admin");
            } else{
                author.getRoles().remove(roleRepository.findByName("ROLE_USER"));
                author.getRoles().add(roleRepository.findByName("ROLE_ADMIN"));
                authorRepository.save(author);
                LOGGER.debug("Author role changed to admin for {}", author);
                return ResponseEntity.ok().body("Role changed to admin");
            }
        }catch (Exception exception){
            LOGGER.error("author does not exist with id {}", id);
            exception.printStackTrace();
            return ResponseEntity.badRequest().body("author does not exist with id :" + id);
        }
    }

    public ResponseEntity<?> deleteAuthorById(Long id){
        LOGGER.debug("rendering deleteAuthorById");
        try {
            passwordTokenRepository.deleteByAuthor(authorRepository.findById(id).get());
            verificationTokenRepository.deleteByAuthor(authorRepository.findById(id).get());
            authorRepository.deleteById(id);
            LOGGER.debug("author with id {} deleted",id);
            return ResponseEntity.ok().body("deleted successfully");
        }catch (Exception exception){
            LOGGER.error("author does not exist with id {}", id);
            exception.printStackTrace();
            return ResponseEntity.badRequest().body("Author does not exist with id "+id);
        }
    }

    public Author registerNewUserAccount(final AuthorDto accountDto) {
        if (emailExists(accountDto.getEmail())) {
            return null;
        }
        final Author author = new Author();
        author.setName(accountDto.getName());
        author.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        author.setEmail(accountDto.getEmail());
        author.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
        return authorRepository.save(author);
    }

    public Author getUser(final String verificationToken) {
        final VerificationToken token = verificationTokenRepository.findByToken(verificationToken);
        if (token != null) {
            return token.getAuthor();
        }
        return null;
    }

    public VerificationToken getVerificationToken(final String VerificationToken) {
        return verificationTokenRepository.findByToken(VerificationToken);
    }

    public void saveRegisteredUser(final Author author) {
        authorRepository.save(author);
    }

    public void createVerificationTokenForUser(final Author author, final String token) {
        final VerificationToken myToken = new VerificationToken(token, author);
        verificationTokenRepository.save(myToken);
    }

    public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
        VerificationToken vToken = verificationTokenRepository.findByToken(existingVerificationToken);
        vToken.updateToken(UUID.randomUUID()
            .toString());
        vToken = verificationTokenRepository.save(vToken);
        return vToken;
    }

    public void createPasswordResetTokenForUser(final Author author, final String token) {
        final PasswordResetToken myToken = new PasswordResetToken(token, author);
        passwordTokenRepository.save(myToken);
    }

    public Author findUserByEmail(final String email) {
        return authorRepository.findByEmail(email);
    }

    public PasswordResetToken getPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token);
    }

    public void changeUserPassword(final Author author, final String password) {
        author.setPassword(passwordEncoder.encode(password));
        authorRepository.save(author);
    }

    public ResponseEntity<?> getUserById(final Long id){
        LOGGER.debug("rendering getUserById");
        if(authorRepository.findById(id).isPresent()){
            return ResponseEntity.ok().body(authorRepository.findById(id).get());
        } else {
            LOGGER.error("no author present with id {}",id);
            return ResponseEntity.badRequest().body("No author present with id "+ id);
        }
    }

    private boolean emailExists(final String email) {
        return authorRepository.findByEmail(email) != null;
    }
}
