package com.example.jpa.repository;

import com.example.jpa.model.Author;
import com.example.jpa.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.stream.Stream;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

    VerificationToken findByAuthor(Author author);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM VerificationToken v where v.author=:author")
    void deleteByAuthor(Author author);

}
