package com.example.jpa.repository;

import com.example.jpa.model.Post;
import com.example.jpa.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    @Query(value = "SELECT t.posts FROM Tag t where t.id =:tagId")
    List<Post> findPostByTagId(Long tagId);

}
