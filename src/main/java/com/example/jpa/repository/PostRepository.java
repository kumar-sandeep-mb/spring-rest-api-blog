package com.example.jpa.repository;

import com.example.jpa.model.Author;
import com.example.jpa.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(value = "SELECT p FROM Post p WHERE p.isPublished = true and (p.title LIKE %:searchString% or p.description LIKE %:searchString% or" +
            " p.content LIKE %:searchString% or p.author.name LIKE %:searchString%)")
    Page<Post> findPostBySearch(String searchString,Pageable pageable);

    @Query(value = "SELECT p FROM Post p WHERE p.isPublished = true and p.author=:author")
    Page<Post> findPostByAuthor(Author author,Pageable pageable);

    @Query(value = "SELECT p FROM Post p WHERE p.isPublished = true")
    Page<Post> findAllPublishedPost(Pageable pageable);

    @Query(value = "SELECT p FROM Post p WHERE p.isPublished = false and p.author=:author")
    Page<Post> findDraftByAuthor(Author author,Pageable pageable);


    Post findByTitle(String title);
}
