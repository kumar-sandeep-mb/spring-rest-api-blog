package com.example.jpa.repository;

import com.example.jpa.model.Author;
import com.example.jpa.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    PasswordResetToken findByToken(String token);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM PasswordResetToken p where p.author=:author")
    void deleteByAuthor(Author author);
}
