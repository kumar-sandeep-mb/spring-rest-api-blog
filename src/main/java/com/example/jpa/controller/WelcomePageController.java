package com.example.jpa.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomePageController {

    @RequestMapping("/")
    public String showWelcomePage(){
        return "You have successfully landed to the welcome page";
    }
}
