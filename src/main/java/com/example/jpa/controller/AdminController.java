package com.example.jpa.controller;

import com.example.jpa.repository.RoleRepository;
import com.example.jpa.service.AuthorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AdminController {
    @Autowired
    AuthorService authorService;
    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/admin/author")
    @ApiOperation(value = "show all author",
    notes = "only admin can access this")
    public ResponseEntity<?> showAuthor(){
        return ResponseEntity.ok().body(authorService.getAll());
    }

    @GetMapping("/admin/author/{id}")
    @ApiOperation(value = "show author by id",
    notes = "provide and author id in the path")
    public ResponseEntity<?> showAuthorById(@PathVariable Long id){
        return authorService.getUserById(id);
    }
    @ApiOperation(value = "delete an author",
            notes = "provide and author id in the path")
    @DeleteMapping("/admin/author/{id}/delete")
    public ResponseEntity<?> deleteAuthor(@PathVariable Long id){
        return authorService.deleteAuthorById(id);
    }

    @PutMapping("/admin/author/{id}/make-admin")
    @ApiOperation(value = "make an author admin",
            notes = "provide and author id in the path")
    public ResponseEntity<?> makeAdmin(@PathVariable Long id){
        System.out.println(id);
        return  authorService.makeAdmin(id);
    }
}
