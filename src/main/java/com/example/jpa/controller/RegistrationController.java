package com.example.jpa.controller;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.repository.VerificationTokenRepository;
import com.example.jpa.service.RegistrationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class RegistrationController {

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private RegistrationService registrationService;

    public RegistrationController() {
        super();
    }

    @PostMapping("/sign-up")
    @ApiOperation(value = "create a new account")
    public ResponseEntity<?> registerUserAccount(@RequestBody final AuthorDto authorDto,
                                                 final HttpServletRequest request) {
        return registrationService.registerAccount(authorDto, request);
    }

    @GetMapping("/registrationConfirm")
    @ApiOperation(value = "verify the account",
            notes = "provide the verification token in the body")
    public ResponseEntity<?> confirmRegistration(@RequestParam final String token) {
        return registrationService.confirmRegister(token);
    }

    @PostMapping("/resendVerificationMail")
    @ApiOperation(value = "resend the verification mail",
            notes = "provide specified mail account in the body")
    public ResponseEntity<?> resendVerificationMail(final HttpServletRequest request, @RequestBody String userEmail) {
        return registrationService.resendVerificationMail(request,userEmail);
    }

    @PostMapping("/resetPassword")
    @ApiOperation(value = "request reset password token",
            notes = "provide the specified mail")
    public ResponseEntity<?> sendPasswordResetToken(final HttpServletRequest request, @RequestParam final String userEmail) {
        return registrationService.sendPasswordResetToken(request, userEmail);
    }

    @PostMapping("/changePassword")
    @ApiOperation(value = "create new password",
            notes = "provide new password and password reset token in the body")
    public ResponseEntity<?> resetPassword(@RequestParam("token") String token,@RequestParam("id")Long id,@RequestParam ("password") String password) {
        return registrationService.updatePassword(token,password);
    }
}
