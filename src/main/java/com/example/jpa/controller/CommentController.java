package com.example.jpa.controller;

import com.example.jpa.model.Comment;
import com.example.jpa.service.CommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/post/{postId}/comment")
    @ApiOperation(value = "show all comments on the post",
            notes = "provide and post id in the path")
    public ResponseEntity<?> showCommentByPost(@PathVariable Long postId){
        return commentService.findCommentByPostId(postId);
    }

    @PostMapping("/post/{postId}/new-comment")
    @ApiOperation(value = "post a new comment on the post",
            notes = "provide and post id in the path")
    public ResponseEntity<?> createComment(@PathVariable(value = "postId") Long postId, @RequestBody Comment comment,
                                Authentication authentication) {
        return commentService.saveComment(comment,authentication,postId);
    }

    @GetMapping("post/{postId}/comment/{commentId}")
    @ApiOperation(value = "show comment by id",
            notes = "provide post id and comment id in the path")
    public ResponseEntity<?> showSingleComment(@PathVariable("commentId") Long commentId, @PathVariable Long postId) {
       return ResponseEntity.ok().body(commentService.findCommentByCommentId(commentId));
    }

    @PutMapping("post/{postId}/comment/{commentId}/update-comment")
    @ApiOperation(value = "update a comment",
            notes = "provide post id and comment id in the path")
    public ResponseEntity<?> updateComment(@PathVariable Long commentId,@RequestBody Comment updatedComment,
                                           Authentication authentication) {
        return commentService.updateComment(updatedComment.getCommentMessage(),commentId,authentication);
    }

    @DeleteMapping("post/{postId}/comment/{commentId}/delete-comment")
    @ApiOperation(value = "delete a comment",
            notes = "provide post id and comment id in the path")
    public ResponseEntity<?> deleteComment(@PathVariable Long commentId, Authentication authentication) {
        return commentService.deleteComment(commentId,authentication);
    }
}
