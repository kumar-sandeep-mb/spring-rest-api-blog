package com.example.jpa.controller;

import com.example.jpa.dto.AuthorDto;
import com.example.jpa.service.AuthorService;
import com.example.jpa.service.RegistrationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class AuthorController {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    AuthorService authorService;

    @PutMapping("/author/updateSetting")
    @ApiOperation(value = "update author setting ",
            notes = "author must log in to update setting")
    public ResponseEntity<?> updateAuthorSetting(@RequestBody AuthorDto authorDto, Authentication authentication,
                                              HttpServletRequest request){
        return registrationService.updateAuthorSetting(authorDto,authentication, request);
    }
}
