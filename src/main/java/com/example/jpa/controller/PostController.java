package com.example.jpa.controller;

import com.example.jpa.model.Post;
import com.example.jpa.service.PostService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping("/post")
    @ApiOperation(value = "show all the post")
    public ResponseEntity<?> showAllPost(@RequestParam(defaultValue = "0") int page){
        return postService.getAllPost(page);
    }

    @GetMapping("/post/{postId}")
    @ApiOperation(value = "show post by id",
            notes = "provide post id in the path")
    public ResponseEntity<?> showPostById(@PathVariable Long postId){
        return postService.findById(postId);
    }

    @GetMapping("/post/search-post")
    @ApiOperation(value = "search post",
            notes = "provide search text in the path")
    public ResponseEntity<?> showPostBySearch(@RequestParam(defaultValue = "0") int page,@RequestParam String searchString){
        return postService.getPostBySearch(searchString,page);
    }

    @GetMapping("/post/filter-post/{tagId}")
    @ApiOperation(value = "filter post by tag",
            notes = "provide tag id in the path")
    public ResponseEntity<?> showFilteredPost(@RequestParam(defaultValue = "0") int page,@PathVariable Long tagId){
        return postService.getPostByFilter(tagId,page);
    }

    @GetMapping("/author/post")
    @ApiOperation(value = "show post by author",
            notes = "author must log in to show his all post")
    public ResponseEntity<?> showPostByAuthor(@RequestParam(defaultValue = "0") int page,
                                              Authentication authentication){
        return postService.getPostByAuthor((User)authentication.getPrincipal(),page);
    }

    @GetMapping("/author/draft")
    @ApiOperation(value = "show draft by author",
            notes = "author must log in to show his all draft")
    public ResponseEntity<?> showDraftByAuthor(@RequestParam(defaultValue = "0") int page,Authentication authentication){
        return postService.getDraftByAuthor((User)authentication.getPrincipal(),page);
    }

    @PostMapping("/author/savePost")
    @ApiOperation(value = "crate a new post",
            notes = "only a verified an log in author can create a post")
    public ResponseEntity<?> savePost (@RequestBody Post post, Authentication authentication) {
        return postService.savePost(post, (User)authentication.getPrincipal());
    }

    @PutMapping("/author/post/{postId}/update-post")
    @ApiOperation(value = "update the post",
            notes = "provide post id in the path")
    public ResponseEntity<?> updatePost(@RequestBody Post updatedPost,@PathVariable Long postId,
                                        Authentication authentication) {
        return postService.updatePost(updatedPost,postId,authentication);
    }

    @DeleteMapping("/author/post/{postId}/delete-post")
    @ApiOperation(value = "delete a post",
            notes = "provide post id in the path")
    public ResponseEntity<?> deleteCountry(@PathVariable Long postId,Authentication authentication){
        return postService.deleteById(postId,authentication);
    }
}
