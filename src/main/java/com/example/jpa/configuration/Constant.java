package com.example.jpa.configuration;

public class Constant {
    public static int INITIAL_PAGE_NUMBER = 0;
    public static int PAGE_SIZE = 4;
    public static int ONE_PAGE = 1;
    public static String API = "/api";
    public static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
}
