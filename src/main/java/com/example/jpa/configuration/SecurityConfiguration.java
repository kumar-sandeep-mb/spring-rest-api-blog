package com.example.jpa.configuration;

import com.example.jpa.util.CustomAuthenticationProvider;
import com.example.jpa.util.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService myUserDetailsService;
    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        authProvider.setUserDetailsService(myUserDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/api/").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()

                .antMatchers("/api/post").permitAll()
                .antMatchers("/api/post/*").permitAll()
                .antMatchers("/api/post/*/comment").permitAll()
                .antMatchers("/api/post/*/comment/*").permitAll()
                .antMatchers("/api/post/filter-post/*").permitAll()
                .antMatchers("/api/post/search-post/*").permitAll()
                .antMatchers("/api/post/*/new-comment").authenticated()
                .antMatchers("/api/post/*/comment/*/update-comment").authenticated()
                .antMatchers("/api/post/*/comment/*/delete-comment").authenticated()


                .antMatchers("/api/sign-up").permitAll()
                .antMatchers("/api/resendVerificationMail").permitAll()
                .antMatchers("/api/registrationConfirm").permitAll()
                .antMatchers("/api/resetPassword").permitAll()
                .antMatchers("/api/changePassword").permitAll()

                .antMatchers("/api/author/post").authenticated()
                .antMatchers("/api/author/draft").authenticated()
                .antMatchers("/api/author/savePost").authenticated()
                .antMatchers("/api/author/post/*/update-post").authenticated()
                .antMatchers("/api/author/post/*/delete-post").authenticated()

                .antMatchers("/api/setting").authenticated()

                .antMatchers("/admin/author").hasAuthority("WRITE_PRIVILEGE")
                .antMatchers("/api/admin/author/*").hasAuthority("WRITE_PRIVILEGE")
                .antMatchers("/api/admin/author/*/delete").hasAuthority("WRITE_PRIVILEGE")
                .antMatchers("/api/admin/author/*/make-admin").hasAuthority("WRITE_PRIVILEGE")

//                .antMatchers("/authenticate").permitAll()
                .antMatchers("/api/authenticate").permitAll().

                anyRequest().authenticated().and().
                exceptionHandling().and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**","/webjars/**");
    }

}